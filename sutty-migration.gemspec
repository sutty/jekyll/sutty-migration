# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'sutty-migration'
  spec.version       = '0.3.3'
  spec.authors       = %w[f]
  spec.email         = %w[f@sutty.nl]

  spec.summary       = 'Jekyll data migration for Sutty'
  spec.description   = 'Takes datafiles and converts them into posts'
  spec.homepage      = "https://0xacab.org/sutty/jekyll/#{spec.name}"
  spec.license       = 'GPL-3.0'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.6.0')

  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.files         = Dir['lib/**/*']
  spec.require_paths = %w[lib]

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.add_dependency 'jekyll', '~> 4'
  spec.add_dependency 'jekyll-write-and-commit-changes', '~> 0.1'
  spec.add_dependency 'fast_blank', '~> 1.0'
  spec.add_dependency 'faraday', '~> 1.4'
  spec.add_dependency 'progressbar', '~> 1.11'
  spec.add_dependency 'sqlite3', '~> 1.4'
  spec.add_dependency 'sequel', '~> 5.45'
  spec.add_dependency 'wordpress-formatting', '~> 0.1.0'
  spec.add_dependency 'nokogiri', '~> 1.11'
  spec.add_dependency 'php-serialize', '~> 1.3.0'

  spec.add_development_dependency 'pry'
end
