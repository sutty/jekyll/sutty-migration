# frozen_string_literal: true

# Expandir String para poder verificar si está vacía
require 'fast_blank'

# Verificar que los valores nulos estén vacíos
class NilClass
  def blank?
    true
  end

  def present?
    false
  end
end

# Verificar que una fecha está vacía
class Time
  def blank?
    false
  end

  def present?
    true
  end
end
