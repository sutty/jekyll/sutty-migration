# frozen_string_literal: true

require 'time'
require 'securerandom'
require 'sequel'
require 'sqlite3'
require 'json'
require 'faraday'
require 'progressbar'
require 'jekyll/utils'

module SuttyMigration
  # Brings posts and attachments from a SQLite3 database.  You can
  # convert a MySQL/MariaDB dump by using `mysql2sqlite`.
  #
  # It doesn't convert them into Jekyll posts but allows you to write a
  # migration plugin where you can convert data by yourself.  We may add
  # this feature in the future.
  class Wordpress
    attr_reader :site, :prefix, :limit, :url, :wp, :database, :multisite

    # @param :site [Jekyll::Site] Jekyll site
    # @param :url [String] Wordpress site URL (must be up for downloads)
    # @param :database [String] Database path, by default `_data/wordpress.sqlite3`
    # @param :prefix [String] WP table prefix
    # @param :limit [Integer] Page length
    # @param :multisite [Boolean] Site is multisite
    def initialize(site:, url:, database: nil, prefix: 'wp_', limit: 10, multisite: nil)
      @site = site
      @prefix = prefix.freeze
      @limit = limit.freeze
      @url = url.freeze
      @database = database || File.join(site.source, '_data', 'wordpress.sqlite3')
      @multisite = multisite
    end

    # Generate database connections for a multisite WP
    #
    # @return [Hash] { "ID" => SuttyMigration::Wordpress }
    def blogs
      @blogs ||= wp["select * from #{prefix}blogs"].to_a.map do |blog|
        url   = "https://#{blog[:domain]}#{blog[:path]}"
        pfx   = "#{prefix}#{blog[:blog_id]}_" if blog[:blog_id] > 1
        pfx ||= prefix

        [blog[:blog_id],
         blog.merge(db: self.class.new(site: site, url: url, prefix: pfx, database: database, limit: limit,
                                       multisite: self))]
      end.to_h
    end

    def options
      @options ||= wp["select option_name, option_value from #{prefix}options"].to_a.map(&:values).to_h.transform_keys(&:to_sym)
    end

    # Open the database.
    #
    # @return [Sequel::SQLite::Database]
    def wp
      @wp ||= Sequel.sqlite(database).tap do |db|
        db.extension :pagination
      end
    end

    # Download all attachments.  Adds the local path to them.
    #
    # @param :progress [Boolean] Toggle progress bar
    # @return [Nil]
    def download_all(progress: true)
      posts(layout: 'attachment').each do |attachment|
        attachment[:front_matter]['file_path'] = download(url: attachment[:guid], progress: progress)
      end
    end

    # Downloads a file if needed, optionally showing a progress bar.
    #
    # @param :url [String] File URL
    # @param :progress [Boolean] Toggle progress bar
    # @return [String] File local path
    def download(url:, progress: true)
      uri = URI(url)
      dest = uri.path.sub(%r{\A/}, '')
      full = File.join(site.source, dest)

      return dest if File.exist? full

      ::Jekyll.logger.info "Downloading #{dest}"

      FileUtils.mkdir_p File.dirname(full)

      File.open(full, 'w') do |f|
        if progress
          head = Faraday.head(url)
          content_length = head.headers['content-length'].to_i
          progress = ProgressBar.create(title: File.basename(dest), total: content_length, output: $stderr)
        end

        Faraday.get(url) do |req|
          req.options.on_data = proc do |chunk, downloaded_bytes|
            f.write chunk

            if progress
              progress.progress = downloaded_bytes > content_length ? content_length : downloaded_bytes
            end
          end
        end
      end

      dest
    end

    # List post types
    #
    # @return [Array]
    def layouts
      @layouts ||= wp["select distinct post_type from #{prefix}posts"].to_a.map(&:values).flatten
    end

    # Finds all posts optionally filtering by post type.  This is not
    # the official Sequel syntax, but it retrieves metadata as objects
    # with a single query (and a sub-query).
    #
    # @param :layout [String] Layout name, one of #layouts
    # @param :with_meta [Boolean] Toggle metadata pulling and conversion
    # @return [Enumerator]
    def posts(**options)
      unless options[:layout].blank? || layouts.include?(options[:layout])
        raise ArgumentError, "#{options[:layout]} must be one of #{layouts.join(', ')}"
      end

      wp[post_query(**options)].each_page(limit).to_a.map(&:to_a).flatten.tap do |p|
        p.map do |post|
          # Sequel parses dates on localtime
          post[:date] = ::Jekyll::Utils.parse_date(post[:date]) unless post[:date].blank?
          unless post[:last_modified_at].blank?
            post[:last_modified_at] =
              ::Jekyll::Utils.parse_date(post[:last_modified_at])
          end

          post[:front_matter] =
            begin
              unless post[:front_matter].blank?
                JSON.parse(post[:front_matter]).transform_keys(&:to_sym).transform_values do |v|
                  v.size == 1 ? v.first : v
                end
              end
            rescue JSON::ParserError
              {}
            end
          post[:terms] =
            begin
              unless post[:terms].blank?
                JSON.parse(post[:terms]).transform_keys(&:to_sym).transform_values do |v|
                  v.size == 1 ? v.first : v
                end
              end
            rescue JSON::ParserError
              {}
            end
        end
      end
    end

    # Brings all users.
    #
    # @param :with_meta [Boolean] include metadata
    # @return [Array]
    def users(**options)
      options[:with_meta] = true unless options.key? :with_meta

      wp[user_query(**options)].each_page(limit).to_a.map(&:to_a).flatten.tap do |u|
        next unless options[:with_meta]

        u.map do |user|
          user[:meta] = JSON.parse(user[:meta]).transform_keys(&:to_sym) unless user[:meta].blank?
        end
      end
    end

    private

    # Finds all users.  If it's a multisite WP, we need to check the
    # main table.
    #
    # @param :with_meta [Boolean] include metadata
    # @return [String]
    def user_query(with_meta: true)
      pfx = multisite&.prefix || prefix

      <<~EOQ
        select
          u.*
          #{', json_group_object(m.meta_key, m.meta_value) as meta' if with_meta}
        from #{pfx}users as u
        #{"left join #{pfx}usermeta as m on m.user_id = u.id" if with_meta}
        group by u.id
      EOQ
    end

    # Query for posts, optionally bringing metadata as JSON objects.
    #
    # @param :layout [String] Layout name
    # @param :with_meta [Boolean] Query metadata
    # @return [String]
    def post_query(layout: nil, with_meta: true)
      <<~EOQ
        select
          p.ID as id,
          strftime('%Y-%m-%d %H:%M:%S UTC', p.post_date_gmt) as date,
          strftime('%Y-%m-%d %H:%M:%S UTC', p.post_modified_gmt) as last_modified_at,
          p.post_author as author,
          p.post_type as layout,
          p.post_name as slug,
          p.post_title as title,
          p.post_content as content,
          p.post_excerpt as excerpt,
          p.post_status as status,
          p.comment_status as comment_status,
          p.ping_status as ping_status,
          p.post_password as password,
          p.to_ping as to_ping,
          p.pinged as pinged,
          p.post_content_filtered as content_filtered,
          p.post_parent as parent,
          p.guid as guid,
          p.menu_order as menu_order,
          p.post_mime_type as mime_type,
          p.comment_count as comment_count
          #{', f.front_matter as front_matter' if with_meta}
          #{', t.terms as terms' if with_meta}
        from #{prefix}posts as p
        #{"left join (#{meta_query(layout: layout)}) as f on f.post_id = p.ID" if with_meta}
        #{"left join (#{terms_query(layout: layout)}) as t on t.post_id = p.ID" if with_meta}
        #{"where p.post_type = '#{layout}'" if layout}
        group by p.ID
      EOQ
    end

    # Recover the post meta as a JSON object with multiple values
    # converted to arrays
    #
    # @return [String]
    def meta_query(layout: nil)
      <<~EOQ
        select
          post_id,
          json_group_object(meta_key, json(meta_values)) as front_matter
        from (
          select
            post_id,
            meta_key,
            json_group_array(meta_value) as meta_values
          from #{prefix}postmeta
          group by post_id, meta_key
        )
        #{"where post_id in (select ID from #{prefix}posts where post_type = '#{layout}')" if layout}
        group by post_id
      EOQ
    end

    # Term taxonomy query
    #
    # @param :layout [String] Layout name
    # @return [String]
    def terms_query(layout: nil)
      <<~EOQ
        select
          post_id,
          json_group_object(taxonomy, json(terms)) as terms
        from (
          select
            r.object_id as post_id,
            tt.taxonomy,
            json_group_array(t.name) as terms
          from #{prefix}term_relationships as r
          left join #{prefix}term_taxonomy as tt on tt.term_taxonomy_id = r.term_taxonomy_id
          left join #{prefix}terms as t on t.term_id = tt.term_id
          #{"where r.object_id in (select ID from #{prefix}posts where post_type = '#{layout}')" if layout}
          group by r.object_id, tt.taxonomy)
        group by post_id
      EOQ
    end
  end
end
