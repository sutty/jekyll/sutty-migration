# frozen_string_literal: true

require 'jekyll/utils'
require_relative '../core_extensions'

module SuttyMigration
  module Jekyll
    module DocumentCreator
      class DocumentExists < ArgumentError; end

      def self.included(base)
        base.class_eval do
          class << self
            # Creates a new document in a collection or fails if it already
            # exists.
            #
            # @param :site [Jekyll::Site] Jekyll site
            # @param :date [Time] Post date
            # @param :title [String] Post title
            # @param :slug [String] Post slug, slugified title if empty
            # @param :collection [Jekyll::Collection,String] Collection label or collection
            # @return [Jekyll::Document] A new document
            def create(site:, date:, title:, collection:, slug: nil)
              collection = site.collections[collection] if collection.is_a? String
              slug = ::Jekyll::Utils.slugify(title, mode: 'latin') if slug.blank?
              basename = "#{date.strftime('%F')}-#{slug}.markdown"
              path = File.join(collection.relative_directory, basename)

              raise DocumentExists, "#{path} already exists" if File.exist? path

              indexed_documents_by_relative_path(site)[path] =
                ::Jekyll::Document.new(path, site: site, collection: collection).tap do |document|
                  collection.docs << document
                  document.data['title'] = title
                end
            end

            # Finds a document by its relative path or creates it if it
            # doesn't exist.  Helpful for idempotent migrations (create or
            # update actions)
            #
            # @param :site [Jekyll::Site] Jekyll site
            # @param :date [Time] Post date
            # @param :title [String] Post title
            # @param :slug [String] Post slug, slugified title if empty
            # @param :collection [Jekyll::Collection,String] Collection label or collection
            # @return [Jekyll::Document] The found document or a new one
            def find_or_create(site:, date:, title:, collection:, slug: nil)
              collection = site.collections[collection] if collection.is_a? String
              slug = ::Jekyll::Utils.slugify(title, mode: 'latin') if slug.blank?
              basename = "#{date.strftime('%F')}-#{slug}.markdown"
              path = File.join(collection.relative_directory, basename)

              return find(site: site, relative_path: path) if File.exist?(path)

              create(site: site, date: date, title: title, slug: slug, collection: collection)
            end

            # Finds a document by its relative path
            #
            # @param :site [Jekyll::Site]
            # @param :relative_path [String]
            # @return [Jekyll::Document,Nil]
            def find(site:, relative_path:)
              indexed_documents_by_relative_path(site)[relative_path]
            end

            # Index documents by relative path for faster finding
            #
            # @param [Jekyll::Site]
            # @return [Hash]
            def indexed_documents_by_relative_path(site)
              @indexed_documents_by_relative_path ||= site.documents.reduce({}) do |idx, doc|
                idx.tap do |i|
                  i[doc.relative_path] = doc
                end
              end
            end
          end
        end
      end
    end
  end
end

::Jekyll::Document.include SuttyMigration::Jekyll::DocumentCreator
