# frozen_string_literal: true

require 'wordpress_formatting/wpautop'
require 'jekyll/utils'

module SuttyMigration
  class WordpressXml
    # Represents a WordPress post
    class Post
      attr_reader :wordpress, :item

      # @param :wordpress [SuttyMigration::WordpressXml]
      # @param :item [Nokogiri::XML::Element]
      def initialize(wordpress:, item:)
        @wordpress = wordpress
        @item = item
      end

      def inspect
        "#<SuttyMigration::WordpressXml::Post title=\"#{title}\">"
      end

      # Post ID
      #
      # @return [Integer]
      def id
        @id ||= attribute_value('post_id').to_i
      end

      # Permalink. Absolute URL to the post.
      #
      # @return [String]
      def permalink
        @permalink ||= attribute_value('link').sub(wordpress.url, '')
      end

      # Title
      #
      # @return [String]
      def title
        @title ||= attribute_value('title')
      end

      # Description
      #
      # @return [String]
      def description
        @description ||= attribute_value('description')
      end

      # Slug ("post name")
      #
      # @return [String]
      def slug
        @slug ||= attribute_value('post_name')
      end

      # Publication date.
      #
      # WordPress can store this date in three different fields and
      # sometimes they come empty or invalid.
      #
      # @return [Time]
      def date
        @date ||= %w[pubDate post_date_gmt post_date].map do |date_attr|
          ::Jekyll::Utils.parse_date attribute_value(date_attr)
        rescue StandardError
        end.compact.first
      end

      # Modification date.
      #
      # @return [Time]
      def last_modified_at
        @last_modified_at ||= ::Jekyll::Utils.parse_date attribute_value('post_modified_gmt')
      end

      # Content as HTML, with site URL removed.
      #
      # @return [String]
      def content
        @content ||= WordpressFormatting::Wpautop.wpautop(attribute_value('encoded')).gsub(
          / (href|src)="#{wordpress.url}/, ' \\1="'
        )
      end

      # Author attributes.
      #
      # @return [Hash]
      def author
        @author ||= wordpress.authors[attribute_value('creator')]
      end

      # Post password.  Use with jekyll-crypto.
      #
      # @return [String]
      def password
        @password ||= attribute_value 'post_password'
      end

      # Tags with attributes.
      #
      # @return [Hash]
      def tags
        @tags ||= item.css('category').select do |c|
          c[:domain] == 'post_tag'
        end.map do |c|
          wordpress.tags[c[:nicename]]
        end
      end

      # Categories with attributes.
      #
      # @return [Hash]
      def categories
        @categories ||= item.css('category').select do |c|
          c[:domain] == 'category'
        end.map do |c|
          wordpress.categories[c[:nicename]]
        end
      end

      # Metadata.  Plugins store useful information here.  Duplicated
      # keys are returned as an Array of values.
      #
      # @return [Hash]
      def meta
        @meta ||= {}.tap do |meta|
          item.css('postmeta').each do |m|
            key = m.css('meta_key').text
            value = m.css('meta_value').text

            case meta[key]
            when nil then meta[key] = value
            when String then meta[key] = [meta[key], value]
            when Array then meta[key] << value
            end
          end
        end
      end

      # Order.  Higher are sorted on top by jekyll-order.
      #
      # @return [Integer]
      def order
        @order ||= attribute_value 'is_sticky'
      end

      # Publication status
      #
      # @return [Boolean]
      def published?
        @published ||= attribute_value('status') == 'publish'
      end

      # Publication status
      #
      # @return [Boolean]
      def draft?
        @draft ||= attribute_value('status') == 'draft'
      end

      # Get a value from the attribute
      #
      # @return [String]
      def attribute_value(key)
        item.at_css(key).text
      end
    end
  end
end
