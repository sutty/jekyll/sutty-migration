# frozen_string_literal: true

require_relative 'post'
require 'php-serialize'
require 'faraday'
require 'progressbar'

module SuttyMigration
  class WordpressXml
    # Represents an attachment or uploaded file.
    class Attachment < Post
      # File URL
      #
      # @return [String]
      def attachment_url
        @attachment_url ||= attribute_value 'attachment_url'
      end

      # File destination
      #
      # @return [String]
      def dest
        @dest ||= URI(attachment_url).path.sub(%r{\A/}, '')
      end

      # Metadata, with file information as a Hash
      #
      # @return [Hash]
      def meta
        super.tap do |m|
          m['_wp_attachment_metadata'] = PHP.unserialize m['_wp_attachment_metadata']
        end
      end

      # Download the file if it doesn't exist.  Optionally show a
      # progress bar.
      #
      # @param :progress [Boolean]
      # @return [Boolean]
      def download(progress: true)
        return true if File.exist? dest

        ::Jekyll.logger.info "Downloading #{dest}"

        FileUtils.mkdir_p File.dirname(dest)

        File.open(dest, 'w') do |f|
          if progress
            head = Faraday.head(attachment_url)
            content_length = head.headers['content-length'].to_i
            progress = ProgressBar.create(title: File.basename(dest), total: content_length, output: $stderr)
          end

          Faraday.get(attachment_url) do |req|
            req.options.on_data = proc do |chunk, downloaded_bytes|
              f.write chunk

              if progress
                progress.progress = downloaded_bytes > content_length ? content_length : downloaded_bytes
              end
            end
          end
        end

        File.exist? dest
      end
    end
  end
end
