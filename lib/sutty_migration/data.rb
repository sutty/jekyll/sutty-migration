# frozen_string_literal: true

require 'securerandom'
require_relative 'core_extensions'
require_relative 'jekyll/document_creator'

# Registers a plugin for converting CSV files into posts following
# Sutty's layout definition.
#
# If jekyll-write-and-commit-changes is enabled, documents will be saved
# on disk and commited is the build command is run with
# JEKYLL_ENV=production
Jekyll::Hooks.register :site, :post_read, priority: :low do |site|
  documents = site.documents

  array_separator = site.config.fetch('array_separator', ',')

  site.data['layouts']&.each do |name, layout|
    site.data.dig('migration', name)&.each do |row|
      row['date'] = Jekyll::Utils.parse_date(row['date']) unless row['date'].blank?
      row['date'] ||= Time.now

      unless row['id'].blank?
        document = documents.find do |doc|
          doc.data['id'] == row['id']
        end
      end

      document ||= begin
                     data = row.slice(*%w[date slug title]).transform_keys(&:to_sym)
                     Jekyll::Document.find_or_create(site: site, collection: 'posts', **data)
                   end
      next unless document

      row.each do |attribute, value|
        next if value.nil? || value.blank?

        value.strip! if value.is_a? String

        row[attribute] =
          case layout.dig(attribute, 'type')
          when 'string' then value.tr("\n", ' ').squeeze(' ')
          when 'text' then value.gsub("\n", "\n\n")
          when 'tel' then value.tr("\n", ' ').squeeze(' ')
          # TODO: validate
          when 'color' then value.tr("\n", ' ').squeeze(' ')
          when 'date' then Jekyll::Utils.parse_date(value)
          # TODO: validate
          when 'email' then value.tr("\n", ' ').squeeze(' ')
          # TODO: validate
          when 'url' then value.tr("\n", ' ').squeeze(' ')
          when 'content' then value.gsub("\n", "\n\n")
          when 'markdown_content' then value.gsub("\n", "\n\n")
          when 'markdown' then value.gsub("\n", "\n\n")
          when 'number' then value.to_i
          when 'order' then value.to_i
          when 'boolean' then !value.strip.empty?
          when 'array' then value.split(array_separator).map(&:strip)
          # TODO: process values from the default array
          when 'predefined_array' then value.split(array_separator).map(&:strip)
          when 'image' then { 'path' => value, 'description' => '' }
          when 'file' then { 'path' => value, 'description' => '' }
          when 'geo' then %w[lat lng].zip(value.split(array_separator, 2).map(&:to_f)).to_h
          when 'belongs_to' then value
          when 'has_many' then value.split(array_separator).map(&:strip)
          when 'has_and_belongs_to_many' then value.split(array_separator).map(&:strip)
          when 'related_posts' then value.split(array_separator).map(&:strip)
          when 'locales' then value.split(array_separator).map(&:strip)
          else value
          end
      end

      document.data['uuid'] ||= SecureRandom.uuid
      document.content = row.delete('content')

      document.data.merge! row
      document.save if document.respond_to? :save
    end
  end

  next unless site.respond_to?(:repository)
  next unless ENV['JEKYLL_ENV'] == 'production'

  site.repository.commit 'CSV Migration'
end
