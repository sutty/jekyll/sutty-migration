# sutty-migration

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'sutty-migration'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sutty-migration

## Usage

Add the plugin to your `_config.yml`:

```yaml
plugins:
- sutty-migration
array_separator: ','
```

Compile a CSV file with the following required fields:

```
id,title,date
```

The `date` field is optional, but once they're set they can't change,
otherwise you'll start to find duplicates.

Put the CSV file inside a `_data/migration/` directory and name it as
the layout you're migrating into.  For instance, to migrate posts, use
a file called `_data/migration/post.csv`.

Then, define the layout data by using a layout data file.  You can
create one of these using `sutty-cli`.

```bash
# install sutty-cli
gem install sutty-cli
# create a layout
sutty-cli layout post
# add fields
sutty-cli field description --layout post --type text
```

And now you can add the `description` field into the CSV:

```
id,title,date,description
1,"Article",2020-01-01,"Short description"
```

The ID is important to identify updates, it can be anything, numbers,
letters, etc.  We recommend using unique identifiers like UUIDs.

To start migration just build your site:

```bash
bundle exec jekyll build
```

**Tip:** Files can also be JSON, TSV and YAML, since they're all
supported by Jekyll.

### Wordpress

Instead of requiring you to install and configure MariaDB/MySQL, you can
convert the database into SQLite3 like this:

```bash
git clone https://0xacab.org/sutty/mysql2sqlite.git
cd mysql2sqlite
./mysql2sqlite /path/to/database/dump.sql |
  sed -re "s/, 0x([0-9a-f]+),/, X'\1',/i" |
  sqlite3 wordpress.sqlite3
```

It will probably show some errors.

Note the `sed` command is required to convert hexadecimal values into
SQLite syntax, since `mysql2sqlite` doesn't support this yet.

Wordpress websites can include lots of posts and metadata, depending on
the amount of plugins installed.  We don't have an official way of
dumping everything into Jekyll, because you will probably want to move
things around.  You can write a plugin like this:

```ruby
# _plugins/wordpress.rb
# frozen_string_literal: true

require 'sutty_migration/wordpress'
require 'sutty_migration/jekyll/document_creator'
require 'jekyll-write-and-commit-changes'

Jekyll::Hooks.register :site, :post_read, priority: :low do |site|
  wp = SuttyMigration::Wordpress.new(site: site, database: 'wordpress.sqlite3', prefix: 'wp_', url: 'https://wordpre.ss')

  # Download all files
  wp.download_all

  wp.posts(layout: 'post').each do |post|
    doc = Jekyll::Document.create(site: site, title: post[:post_title], date: post[:post_date], collection: 'posts')
    doc.content = post[:content]
    doc.save
  end
end
```

### WordPress XML

If you have the XML dump from a WordPress site, you can migrate content
by writing a migration plugin.

```ruby
# frozen_string_literal: true

require 'sutty_migration/jekyll/document_creator'
require 'sutty_migration/wordpress_xml'
require 'jekyll-write-and-commit-changes'
require 'securerandom'

# Run after reading the site
Jekyll::Hooks.register :site, :post_read do |site|
  # Put the XML dump at _files/wordpress.xml
  xml = SuttyMigration::WordpressXml.new site: site, file: '_files/wordpress.xml'

  # Download all files
  xml.attachments.values.map(&:download)

  # Migrate posts.  You can move metadata around and recover
  # relationships or any info your theme requires.
  xml.posts.values.each do |post|
    # Update documents already migrated.
    doc = Jekyll::Document.find_or_create(site: site, collection: locale, title: post.title, slug: post.slug, date: post.date)
    # Don't change the UUIDv4
    d.data['uuid'] ||= SecureRandom.uuid
    d.data['draft'] = post.draft?
    d.data['layout'] = 'post'
    d.data['last_modified_at'] = post.last_modified_at

    d.data['categories'] = post.categories.map { |c| c[:title] }
    d.data['tags'] = post.tags.map { |t| t[:title] }

    d.data['author'] = post.author[:email]
    d.data['description'] = post.description
    d.content = post.content

    doc.save
  rescue => e
    Jekyll.logger.warn "Couldn't migrate #{post.title}"
  end

  exit # Stop here
end
```


## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/sutty-migration>. This
project is intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the sutty-migration project’s codebases, issue
trackers, chat rooms and mailing lists is expected to follow the [code
of conduct](https://sutty.nl/en/code-of-conduct/).
